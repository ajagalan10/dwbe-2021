const calculadora = require("./calculator")
num1 = 5;
num2 = 2;

const sumar = `${num1} + ${num2} = ` + `${calculadora.Sumar(num1,num2)}`;
console.log(calculadora.Sumar (num1, num2))

const restar = `${num1} - ${num2} = ` + `${calculadora.Restar(num1,num2)}`;
console.log(calculadora.Restar(num1,num2))

const multiplicar = `${num1} * ${num2} = ` + `${calculadora.Multiplicar(num1,num2)}`;
console.log(calculadora.Multiplicar(num1,num2))

const dividir = `${num1} / ${num2} = ` + `${calculadora.Dividir(num1,num2)}`;
console.log(calculadora.Dividir(num1,num2))

const fs = require('fs');
fs.writeFile('./guardaResultCalculadora.txt', ` ${sumar}\n ${restar}\n ${dividir}\n ${multiplicar}`, (err) => {
    if (err) console.log(err);
    else console.log("Archive TXT creado.");
});