const datos = [{
    id: 1,
    nombre: "Jorge Luis",
    apellido: "Borges",
    fechaDeNacimiento: "24/08/1899",
    libros: [{
            id: 1,
            titulo: "Ficciones",
            descripcion: "cuentos",
            anioPublicacion: 1944
        },
        {
            id: 2,
            titulo: "El Aleph",
            descripcion: "otro libro de cuentos",
            anioPublicacion: 1949
        }
    ]
},{
    id: 2,
    nombre: "Gabriel",
    apellido: "Garcia Marquez",
    fechaDeNacimiento: "06/03/1927",
    libros: [{
            id: 1,
            titulo: "Cien años de soledad",
            descripcion: "novela",
            anioPublicacion: 1967
        },
        {
            id: 2,
            titulo: "El amor en tiempos del colera",
            descripcion: "novela",
            anioPublicacion: 1985
        },
        {
            id: 3,
            titulo: "Cronica de una muerte anunciada",
            descripcion: "novela",
            anioPublicacion: 1981
        },
        {
            id: 4,
            titulo: "El coronel no tiene quien le escriba",
            descripcion: "novela",
            anioPublicacion: 1981
        }
    ]
},{
    id: 3,
    nombre: "Fiódor",
    apellido: "Dostoyevski",
    fechaDeNacimiento: "11/11/1821",
    libros: [{
            id: 1,
            titulo: "Crimen y castigo",
            descripcion: "novela",
            anioPublicacion: 1866
        },
        {
            id: 2,
            titulo: "Los hermanos Karamazov",
            descripcion: "novela",
            anioPublicacion: 1879
        },
        {
            id: 3,
            titulo: "Los endemoniados",
            descripcion: "novela",
            anioPublicacion: 1871
        },
        {
            id: 4,
            titulo: "Memorias del subsuelo",
            descripcion: "novela",
            anioPublicacion: 1864
        }
    ]
},{
    id: 4,
    nombre: "Franz",
    apellido: "Kafka",
    fechaDeNacimiento: "03/07/1883",
    libros: [{
            id: 1,
            titulo: "La metamorfosis",
            descripcion: "novela",
            anioPublicacion: 1915
        },
        {
            id: 2,
            titulo: "El castillo",
            descripcion: "novela",
            anioPublicacion: 1926
        },
        {
            id: 3,
            titulo: "El Desaparecido: (América)",
            descripcion: "novela",
            anioPublicacion: 1927
        },
        {
            id: 4,
            titulo: "The Complete Stories of Franz Kafka",
            descripcion: "novelas",
            anioPublicacion: 1971
        }
    ]
}];

module.exports = datos;