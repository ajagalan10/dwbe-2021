const express = require('express');
const app = express();
const metodos = require('./metodos');
const midd = require('./middlewares');

app.use(express.json());

// Middlewares de validaciones //
const validacionesAutor = [midd.validarIdNumericoAutor, midd.validarIdAutorExistente];
const validacionesLibro = [midd.validarIdNumericoLibro, midd.validarIdLibroExistente];
const validaciones = validacionesAutor.concat(validacionesLibro);
// Rutas Autores //
// Retorna array c/ todos los autores //
app.get('/autores', (req, res) => res.status(200).json(metodos.getAutores()));
// Crea un nuevo autor //
app.post('/autores', (req, res) => res.status(201).json(metodos.postAutor(req)));
// Retorna autor por id //
app.get('/autores/:id', validacionesAutor, (req, res) => res.status(200).json(metodos.getAutor(req)));
// Elimina un autor por id //
app.delete('/autores/:id', validacionesAutor, (req, res) => res.status(200).json(metodos.delAutor(req)));
// Modifica un autor por id //
app.put('/autores/:id', validacionesAutor, (req, res) => res.status(200).json(metodos.putAutor(req)));

// RUTAS LIBROS //
// Retorna todos los libros de un autor //
app.get('/autores/:id/libros', validacionesAutor, (req, res) => res.status(200).json(metodos.getLibrosPorAutor(req)));
// Agrega un nuevo libro p/ el autor //
app.post('/autores/:id/libros', validacionesAutor, (req, res) => res.status(200).json(metodos.postLibroAutor(req)));
// Retorna un libro de un autor segun su id //
app.get('/autores/:id/libros/:idLibro', validaciones, (req, res) => res.status(200).json(metodos.getLibroAutor(req)));
// Modifica libro de un auto segun id //
app.put('/autores/:id/libros/:idLibro', validaciones, (req, res) => res.status(200).json(metodos.putLibroAutor(req)));
// Elimina libro segun su id //
app.delete('/autores/:id/libros/:idLibro', validaciones, (req, res) => res.status(200).json(metodos.delLibroAutor(req)));


app.listen(3000, () => console.log('Escuchando el puerto 3000!'));