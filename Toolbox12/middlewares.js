const autores = require('./datos');

// Valida que el id pasado por parametro sea numerico //
const validarIdNumerico = (id, res, next) => {
    if (Number.isInteger(id)) {
        return next();
    } else {
        return res.status(422).json({'message': 'El id debe ser numerico'});
    }
}

// valida id numerico de autor //
const validarIdNumericoAutor = (req, res, next) => validarIdNumerico(parseInt(req.params.id), res, next);

// valida id numerico de libro //
const validarIdNumericoLibro = (req, res, next) => validarIdNumerico(parseInt(req.params.idLibro), res, next);

// Valida que el id del autor pasado por parametro exista //
const validarIdAutorExistente = (req, res, next) => {   
    let autor = buscarAutor(parseInt(req.params.id));
    validarBusqueda(autor, res, next, `{ "message" : "No existe autor con id ${req.params.id}" }`);
}

// Valida que el id del libro pasado por parametro exista //
const validarIdLibroExistente = (req, res, next) => {
    let idx = buscarIdxAutorEnArray(req);
    let libro = buscarLibro(parseInt(req.params.idLibro), idx);
    validarBusqueda(libro, res, next, `{ "message" : "No existe Libro con id ${req.params.idLibro}" }`);
}

// Retorna el indice de la posicion en el array donde se encuentra el autor //
function buscarIdxAutorEnArray(req) {
    let autor = buscarAutor(parseInt(req.params.id));
    return autores.indexOf(autor);
}
// Busca en array segun id //
const buscar = (array, id) => { return array.find(item => item.id === id); }

// Busca autor por id (el parametro debe ser numerico)//
const buscarAutor = (id) => { return buscar(autores, id); }

// Busca Libro por id (el parametro debe ser numerico)//
const buscarLibro = (id, idx) => { return buscar(autores[idx].libros, id); }

//Valida resultado de la busqueda segun id//
const validarBusqueda = (result, res, next, msj) => {
    if (!result)  {
        return res.status(400).json(msj);
    } else {
        next();        
    } 
}

module.exports = {
    validarIdNumerico, validarIdLibroExistente, validarIdAutorExistente, buscarAutor, buscarLibro, validarIdNumericoAutor,
    validarIdNumericoLibro, buscarIdxAutorEnArray
}
