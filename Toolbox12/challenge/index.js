const express = require('express');
const metodos = require('./metodos');
const app = express();

// Middleware a nivel app //
app.use(metodos.middApp);

// Middleware a nivel route //
app.get('/cursos', metodos.middRoute, (req, res) => res.status(200).send(metodos.getCursos()));

app.listen(3000, () => console.log('Escuchando el puerto 3000!'));
