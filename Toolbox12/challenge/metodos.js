const fs = require('fs');

let cursos = [{
    id: 1,
    nombre: 'node'
},{
    id: 2,
    nombre: 'express'
},{
    id: 3,
    nombre: 'git'
},{
    id: 4,
    nombre: 'docker'
},{
    id: 5,
    nombre: 'JAVA'
},{
    id: 6,
    nombre: 'SQL'
},{
    id: 7,
    nombre: 'MongoDB'
}];

// Metodo que devuelve un array con cursos //
const getCursos = () => { return cursos; }

// Metodo que muestra info guardada por consola o maneja excepcion //
const mostrarInfo = (err, info) => {
    err? console.log(err) : console.log(info);
}

// Metodo que guarda info del request en archivo log //
const guardarLog = (info) => {
    fs.appendFile('./log.txt', info, (err) => {
        mostrarInfo(err, info);
    });
}

// Metodo utilizado como middleware a nivel app //
const middApp = (req, res, next) => {
    let info = `${req.path} -> Guardado con middleware a nivel app.\n`;
    guardarLog(info);
    next();
}

// Metodo utilizado como middleware a nivel route//
const middRoute = (req, res, next) => {
    let info = `${req.path} -> Guardado con middleware a nivel route.\n`;
    guardarLog(info);
    next();
}

module.exports = { getCursos, guardarLog, middApp, middRoute };
