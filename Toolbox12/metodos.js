const autores = require('./datos');
const midd = require('./middlewares');

// Retorna array c/ autores //
const getAutores = () => { return autores }

// Crea nuevo autor //
const postAutor = (req) => {
    autores.push(req.body);
    return {'message': 'Autor creado con exito'};
}

// Retorna autor por id //
const getAutor = (req) => { return midd.buscarAutor(parseInt(req.params.id)); }

// Elimina autor por id pasado por parametro //
const delAutor = (req) => {
    let idx = midd.buscarIdxAutorEnArray(req);
    autores.splice(idx, 1);
    return {'message': 'Autor eliminado correctamente'};
}

// Modifica autor por id //
const putAutor = (req) => {
    let idx = midd.buscarIdxAutorEnArray(req);
    if (req.body.nombre) autores[idx].nombre = req.body.nombre;
    if (req.body.apellido) autores[idx].apellido = req.body.apellido;
    if (req.body.fechaDeNacimiento) autores[idx].fechaDeNacimiento = req.body.fechaDeNacimiento;
    return {'message': 'Autor actualizado correctamente'};
}

// Retorna todos los libros de un autor por su id //
const getLibrosPorAutor = (req) => {
    const autor = midd.buscarAutor(parseInt(req.params.id));
    return autor.libros;
}

// Agrega un libro al array de un autor por id //
const postLibroAutor = (req) => {
    let idx = midd.buscarIdxAutorEnArray(req);
    autores[idx].libros.push(req.body);
    return {'message': 'Libro agregado correctamente'};
}

// Retorna un libro por su id //
const getLibroAutor = (req) => {
    let idx = midd.buscarIdxAutorEnArray(req);
    return midd.buscarLibro(parseInt(req.params.idLibro), idx);
}

// modifica un libro por su id //
const putLibroAutor = (req) => {
    let idx = midd.buscarIdxAutorEnArray(req);
    let libro = midd.buscarLibro(parseInt(req.params.idLibro), idx);
    if (req.body.titulo) libro.titulo = req.body.titulo;
    if (req.body.descripcion) libro.descripcion = req.body.descripcion;
    if (req.body.anioPublicacion) libro.anioPublicacion = req.body.anioPublicacion;
    return { "message" : "Libro actualizado correctamente."}
}

// Elimina libro de autor por id pasado por parametro //
const delLibroAutor = (req) => {
    let idxAutor = midd.buscarIdxAutorEnArray(req);
    let libro = midd.buscarLibro(parseInt(req.params.idLibro), idxAutor);
    let idxLibro = autores[idxAutor].libros.indexOf(libro);
    autores[idxAutor].libros.splice(idxLibro, 1);
    return {'message': 'Autor eliminado correctamente'};
}

module.exports = {
    getAutores, postAutor, getAutor, delAutor, getLibrosPorAutor, postLibroAutor, getLibroAutor, putAutor, putLibroAutor, delLibroAutor
}
