var express = require('express');
var app = express();

let alumnos = [
    {nombre: "Pablo", edad: 32},
    {nombre: "Juan", edad: 31},
    {nombre: "Agustin", edad: 29},
    {nombre: "Carla", edad: 30}
];


let nuevoAlumno = {nombre: "Yasmin", edad: 29};



app.get('/alumnos', function (req, res) {
        res.send(alumnos);           
});


app.post('/alumno', function (req, res){
    alumnos.push(nuevoAlumno);
    res.send(`Usuario ${nuevoAlumno.nombre} agregado`)
});
    

app.listen(3000, function () {
    console.log('Escuchando el puerto 3000 en app.js!');
  });