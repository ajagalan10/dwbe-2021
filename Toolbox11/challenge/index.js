const express = require('express');
const app = express();

const metodos = require('./metodos');

app.get('/celulares', (req, res) => metodos.getCelulares(res));

app.get('/cel-menor-precio', (req, res) => metodos.getCelMenorPrecio(res));

app.get('/cel-mayor-precio', (req, res) => metodos.getCelMayorPrecio(res));

app.get('/cel-por-gama', (req, res) => metodos.celPorGama(res));

app.get('/mitad-array-cel', (req, res) => metodos.mitadArrayCel(res));

app.listen(3000, () => console.log('Escuchando el puerto 3000!'));
