let celulares = [
    {   marca: 'Samsung',
        gama: 'alta',
        modelo: 'S10',
        pantalla: '15',
        SO: 'Android',
        precio: 10000   },
    {   marca: 'Samsung',
        gama: 'baja',
        modelo: 'S10',
        pantalla: '15',
        SO: 'Android',
        precio: 120000   },
    {   marca: 'Samsung',
        gama: 'media',
        modelo: 'S10',
        pantalla: '15',
        SO: 'Android',
        precio: 4000   },
    {   marca: 'Samsung',
        gama: 'media',
        modelo: 'S10',
        pantalla: '15',
        SO: 'Android',
        precio: 3000   },
    {   marca: 'Samsung',
        gama: 'alta',
        modelo: 'S10',
        pantalla: '15',
        SO: 'Android',
        precio: 11000   },
    {   marca: 'Samsung',
        gama: 'baja',
        modelo: 'S10',
        pantalla: '15',
        SO: 'Android',
        precio: 18000   },
    {   marca: 'Samsung',
        gama: 'baja',
        modelo: 'S10',
        pantalla: '15',
        SO: 'Android',
        precio: 18000   },
];

// Metodo que retorna array de celulares //
const getCelulares = (res) => res.send(celulares);

// Metodo que retorna la mitad de array de celulares //
const mitadArrayCel = (res) => {
    let aux = Math.round(celulares.length / 2);
    let arrayAux = celulares.splice(0, aux);
    res.status(200).send(arrayAux);
}

// Metodo que retorna celular con menor precio //
const getCelMenorPrecio = (res) => {
    let celAux = celulares[0];
    celulares.forEach( cel => {
        if (cel.precio < celAux.precio) celAux = cel;        
    });
    res.send(celAux);
}

// Metodo que retorna celular con mayor precio //
const getCelMayorPrecio = (res) => {
    let celAux = celulares[0];
    celulares.forEach( cel => {
        if (cel.precio > celAux.precio) celAux = cel;        
    });
    res.send(celAux);
}

// Metodo que agrupa celulares por gama //
const celPorGama = (res) => {
    let gamaBaja = [];
    let gamaAlta = [];
    let gamaMedia = [];

    celulares.forEach( cel => {
        switch (cel.gama) {
            case 'baja':
                gamaBaja.push(cel);
                break;
            case 'alta':
                gamaAlta.push(cel);
                break;
            case 'media':
                gamaMedia.push(cel);
                break;
        }
    });

    res.status(200).send({
        gama_baja: gamaBaja,
        gama_media: gamaMedia,
        gama_alta: gamaAlta
    });
}

module.exports = { getCelulares, mitadArrayCel, getCelMenorPrecio, getCelMayorPrecio, celPorGama }
