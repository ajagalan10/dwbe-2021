const chalk = require('chalk');
const error = chalk.bold.red;
const warning = chalk.keyword('magenta');
console.log(error('Codigo Error!'));
console.log(warning('Warning Naranja!'));


const name = 'Sindre';
console.log(chalk.green('Hello %s'), name);
//=> 'Hello Sindre'
